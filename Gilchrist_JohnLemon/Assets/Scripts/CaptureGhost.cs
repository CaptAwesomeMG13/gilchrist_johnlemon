﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureGhost : MonoBehaviour
{
    public Transform enemy;
    public GameEnding gameEnding;
    public int enemyCount = 5;
    
    bool m_IsEnemyInRange;

    
    void Update ()
    {
        capture();
        if (enemyCount == 0)
        {
            gameEnding.CaughtGhost();
        }
    }


    void capture ()
    {
        if (m_IsEnemyInRange && enemy!=null)
        {
            Vector3 direction = enemy.position - transform.position + Vector3.up;
            Ray ray = new Ray(transform.position, direction);
            RaycastHit raycastHit;

            if(Physics.Raycast(ray, out raycastHit))
            {
                if (raycastHit.collider.transform == enemy)
                {
                    Destroy(enemy.gameObject);
                    enemyCount -= 1;
                }
            }
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            m_IsEnemyInRange = true;
            enemy = other.transform; 
        }
    }

    void OnTriggerExit (Collider other)
    {
        if (other.transform == enemy)
        {
            m_IsEnemyInRange = false;
            enemy = null;
        }
    }
}