﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f;
    public int maxHealth;
    float sprintEnergy = 1f;
    int health;
    Animator m_Animator;
    Rigidbody m_Rigidbody;
    AudioSource m_AudioSource;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    public GameObject VacParticles;
    public Slider healthBar;

    void Start ()
    {
        m_Animator = GetComponent<Animator> ();
        m_Rigidbody = GetComponent<Rigidbody> ();
        m_AudioSource = GetComponent<AudioSource> ();
        VacParticles.SetActive(false);
        health = maxHealth;
        healthBar.maxValue = maxHealth;
        setHealthSlider();
    }

    void FixedUpdate ()
    {
        walking();

    }

    void Update ()
    {
        sprinting();
        shooting();

    }

    void OnAnimatorMove ()
    {
        m_Rigidbody.MovePosition (m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation (m_Rotation);
    }

    //Walking function
    void walking ()
    {
        float horizontal = Input.GetAxis ("Horizontal");
        float vertical = Input.GetAxis ("Vertical");
        
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize ();

        bool hasHorizontalInput = !Mathf.Approximately (horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately (vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        m_Animator.SetBool ("IsWalking", isWalking);
        
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop ();
        }

        Vector3 desiredForward = Vector3.RotateTowards (transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation (desiredForward);
    }

    //Sprinting function
    void sprinting ()
    {
        if (Input.GetKey(KeyCode.LeftShift) && sprintEnergy > 0f)
        {
            m_Animator.speed = 2f;
            sprintEnergy -= Time.deltaTime;
        }
        else
        {
            m_Animator.speed = 1f;
            sprintEnergy += Time.deltaTime * .25f;
            if (sprintEnergy > 1f)
            {
                sprintEnergy = 1f;
            }
        }
    }

    //Turn on the vacuum function
    void shooting ()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            VacParticles.SetActive(true);
        }
        else
        {
            VacParticles.SetActive(false);
        }
    }

    void setHealthSlider ()
    {
        healthBar.value = health;
    }
}